import { readFileSync } from 'fs';
import { ApolloServer } from 'apollo-server';

let TODOS;
let USERS;

const INITIAL_TODOS = [
  { id: '0', action: 'Eat Ramen', completed: false },
  { id: '1', action: 'Save the world', completed: false },
  { id: '2', action: 'Play soccer', completed: true },
];
const INITIAL_USERS = [
  { id: '0', name: 'Naruto', password: '123' },
  { id: '1', name: 'Goku', password: '123' },
  { id: '2', name: 'Oliver', password: '123' },
];

function initTodos() {
  TODOS = [...INITIAL_TODOS];
}
function initUsers() {
  USERS = [...INITIAL_USERS];
}

function byId(id) {
  return x => x.id === id;
}

function getNextId() {
  const last = TODOS.map(x => x.id).sort().pop();
  return (parseInt(last ?? '-1') + 1).toString();
}

async function randomSleep(max = 800) {
  await new Promise(r => setTimeout(r, Math.random() * max));
}

initTodos();
initUsers();

const server = new ApolloServer({
  typeDefs: readFileSync(new URL('schema.graphql', import.meta.url), 'utf-8'),
  context: {
    getTodo(id) {
      const todo = TODOS.find(byId(id));
      if (!todo)
        throw new Error(`TODO ${id} not found`);
      return todo;
    },
    getUser(id) {
      const user = USERS.find(byId(id));
      if (!user)
        throw new Error(`User ${id} not found`);
      return user;
    },
    getUserByNamePassword(name, password) {
      const user = USERS.filter(x => x.name == name && x.password == password);
      if (!user)
        throw new Error(`User ${id} not found`);
      return user;
    },
    async getTodos() {
      await randomSleep();
      return TODOS;
    },
    async getUsers() {
      await randomSleep();
      return USERS;
    },
    async addTodo({ action, completed }) {
      await randomSleep();
      const todo = { id: getNextId(), action, completed };
      TODOS.push(todo);
      return todo;
    },
    async addUser({ name, password }) {
      await randomSleep();
      const user = { id: getNextId(), name, password };
      USERS.push(user);
      return user;
    },

    async updateTodo({ id, action, completed }) {
      await randomSleep();
      const todo = server.context.getTodo(id);
      todo.action = action ?? todo.action;
      todo.completed = completed ?? todo.completed;
      return todo;
    },
    async updateUser({ id, name, password }) {
      await randomSleep();
      const user = server.context.getUser(id);
      user.name = name ?? user.name;
      user.password = password ?? user.password;
      return user;
    },

    async deleteTodo(id) {
      await randomSleep();
      server.context.getTodo(id);
      TODOS = TODOS.filter(x => x.id !== id);
      return TODOS;
    },
    async deleteUser(id) {
      await randomSleep();
      USERS = USERS.filter(x => x.id !== id);
      return USERS;
    },
  },
  resolvers: {
    Query: {
      async todo(_, { todoId }, context) {
        await randomSleep();
        return todoId ? context.getTodo(todoId) : null;
      },
      async user(_, { userId }, context) {
        await randomSleep();
        return userId ? context.getUser(userId) : null;
      },
      async validateUser(_, { name, password }, context) {
        await randomSleep();
        return name && password ? context.getUserByNamePassword(name, password) : null;
      },
      async todos(_, __, context) {
        return context.getTodos();
      },
      async users(_, __, context) {
        return context.getUsers();
      },
    },
    Mutation: {
      async createTodo(_, { input: { action, completed = false } }, context) {
        return context.addTodo({ action, completed });
      },
      async createUser(_, { input: { name, password } }, context) {
        return context.addUser({ name, password });
      },
      async updateTodo(_, { input: { todoId, action, completed } }, context) {
        return context.updateTodo({ id: todoId, action, completed });
      },
      async updateUser(_, { input: { userId, name, password } }, context) {
        return context.updateUser({ id: userId, name, password });
      },
      async toggleTodo(_, { todoId }, context) {
        const todo = await context.getTodo(todoId)
        return context.updateTodo({ ...todo, completed: !todo.completed });
      },
      async deleteTodo(_, { todoId }, context) {
        await context.deleteTodo(todoId);
        return context.getTodos();
      },
      async deleteUser(_, { userId }, context) {
        return await context.deleteUser(userId);
      },
    },
  },
});

// The `listen` method launches a web server.
server.listen().then(({ url }) => {
  console.log(`Server is up and running ${url}`);
});